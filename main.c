/*  
	Copyright (C) 2014 TELECOM NANCY, <http://www.telecomnancy.eu>
		Damien CUPIF
		Nicolas VIARD

	This file is part of UnLeaSH The Kraken, a software meant to provide
	an educational shell helping the students in 1st year at TELECOM Nancy
	to understand the use of basics commands of Linux shell.

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "main.h"
#include "error.h"
#include "readMeta.h"
#include "game.h"
#include "design.h"

    static int loadLevel(char* level);
    static void displayTitle(char* level);
    static int closeGame();

    int main(int argc, char* argv[])
    {
	// Testing syntax of program
    	if (argc!=2) {
    		fprintf(stderr, "Use : %s levelName\n", argv[0]);
    		exit(ERROR_ARGUMENTS);
    	}

    	if(loadLevel(argv[1]) < 0){
    		exit(ERROR_LOADING_LEVEL);
    	}

	// Init variables for level information
    	int nbCommands = 0;
    	char** allowedCommands = NULL;
    	char* secretCode = NULL;
	// Extract information from meta file to setup the level environnement
    	nbCommands = readMeta(&allowedCommands,&secretCode);

    	if(nbCommands < 0){
    		exit(ERROR_READING_META);
    	}

	// We delete the meta file as soon as used in order to avoid any type of cheat from the player
    	if(deleteMeta() < 0){
    		exit(ERROR_DELETING_META);
    	}

    	displayTitle(argv[1]);

	//Launch the game with right environnement
    	if(game(nbCommands, allowedCommands, secretCode) < 0){
    		exit(ERROR_RUNNING_GAME);
    	}

    	freeMeta(nbCommands, allowedCommands, secretCode);

    	if (closeGame()<0) {
    		exit(ERROR_DELETING_GAME_DIRECTORY);
    	}

    	return(0);
    }

    static int loadLevel(char* level){
	// Used to store return values if necessary
    	int returnedValue;

	// Avoid leaving current directory to load level
    	if(strstr(GAME_DIRECTORY, "/") != NULL || strstr(GAME_DIRECTORY, ".") != NULL){
    		fprintf(stderr, "Error, the game directory is not valid.\n");
    		return(-1);
    	}

	// Create an empty "temp" sub-durectory with RWX rights of everyone
    	if (mkdir(GAME_DIRECTORY, 0777) < 0) {
    		perror("Error, the game directory couldn't be created.");
    		return(-1);
    	}

	// Extract the level archive to "temp"
    	returnedValue = fork();
    	if (returnedValue == 0) { 
    		if(execlp("tar", "tar", "-xf", level, "-C", GAME_DIRECTORY, NULL) < 0){
    			perror("Error, the game directory couldn't be extracted.");
    			return(-1);
    		}
    	} else if (returnedValue > 0){
		// Father proc waiting for the end of extraction
    		if(wait(NULL) < 0){
    			perror("Error, the game directory couldn't be extracted.");
    			return(-1);
    		}
    	} else{
    		perror("Error, the game directory couldn't be extracted.");
    		return(-1);
    	}

    	if(chdir(GAME_DIRECTORY) < 0){
    		perror("Error, impossible to go to game directory.");
    		return(-1);
    	}

    	return(0);
    }

    static void displayTitle(char* level){
    	clearScreen();

    	color("1;36");
    	printf("\n                                                                              ,'\"\"`.\n");
    	printf("                       __                                                    / _  _ \\\n");
    	printf(" / /  _   )   _   _   (_ `  )_)   _)_ ( _    _     )_/  _  _  ( _   _   _    |(@)(@)|\n");
    	printf("(_/  ) ) (   )_) (_( .__)  ( (    (_   ) )  )_)   /  ) )  (_(  )\\  )_) ) )   )  __  (\n");
    	printf("            (_                             (_                     (_        /,'))((`.\\\n");
    		printf("                                                                           (( ((  )) ))\n");
    		printf("                                                                            `\\ `)(' /'\n\n");


    		printf("Welcome to the UnLeaSH torture game, you are playing %s\n", level);
    		color("0");
    	}

    	static int closeGame() {
    		if(chdir("..") < 0){
    			perror("Error, impossible to leave the game directory.");
    			return(-1);
    		}
    		int returnedValue = fork();
    		if (returnedValue==0) {
    			if(execlp("rm", "rm", "-r", GAME_DIRECTORY, NULL) < 0){
    				perror("Error, the game directory does not exist.");
    				return(-1);
    			};
    		} else if(returnedValue < 0){
    			perror("Error, impossible to remove the game directory.");
    			return(-1);
    		} else {
    			if(wait(NULL) < 0){
    				perror("Error, impossible to remove the game directory.");
    				return(-1);
    			}
    		}
    		return(0);
    	}