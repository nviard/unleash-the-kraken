/*  
	Copyright (C) 2014 TELECOM NANCY, <http://www.telecomnancy.eu>
		Damien CUPIF
		Nicolas VIARD

	This file is part of UnLeaSH The Kraken, a software meant to provide
	an educational shell helping the students in 1st year at TELECOM Nancy
	to understand the use of basics commands of Linux shell.

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DESIGN_H
#define DESIGN_H

#include <stdio.h>

#define clearScreen() printf("\033[H\033[2J")

#define color(param) printf("\033[%sm",param)

#endif