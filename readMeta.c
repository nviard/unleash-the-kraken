/*  
	Copyright (C) 2014 TELECOM NANCY, <http://www.telecomnancy.eu>
		Damien CUPIF
		Nicolas VIARD

	This file is part of UnLeaSH The Kraken, a software meant to provide
	an educational shell helping the students in 1st year at TELECOM Nancy
	to understand the use of basics commands of Linux shell.

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "error.h"

/* 
	Extract information from meta file to setup the level environnement
	Returns an integer : the number of commands read
*/
int readMeta(char*** allowedCommands, char** secretCode) {

	FILE* meta = NULL;
	char* line = NULL;
	size_t len = 0;

	meta = fopen("meta", "r");
	if (meta!=NULL) {
		int nbCommands = 0;
		char* command;
		unsigned     int i;
		while (getline(&line, &len, meta)!=-1) { // Reading of the file, for each line we apply a different treatment
			// We have to delete the carriage return before we store the command into the bracket otherwise we'll have problems to compare the commands entered by the user
			printf("%s\n", line);
			i=0;
			command = strdup(line+2); // The first two characters are irrelevant according to the syntax of meta file so we'd better not take them into account
			while (*(command+i)!='\n' && i < strlen(command)) {
				i++;
			}
			*(command+i)='\0';

			// According to the first character of each line, we apply the right treatment
			switch (line[0]) {
				case '$':
					// Now we can store the command into the right bracket
					nbCommands++;
					*allowedCommands = (char**) realloc(*allowedCommands, nbCommands * sizeof(char*));
					if(*allowedCommands == NULL){
						perror("Error, impossible to read the allowed commands.");
						return(-1);
					}
					(*allowedCommands)[nbCommands - 1]=command;
					break;
				case '>':
					*secretCode=command;
					break;
				default:
					free(command);
					break;
			}
		}
		free(line);
		fclose(meta);
		return nbCommands;
	} else {
		perror("Error, the meta file does not exist.");
		return(-1);
	}

}

int deleteMeta() {
	int returnedValue = fork();
	if (returnedValue==0) {
		if(execlp("unlink", "unlink", "meta", NULL) < 0){
			perror("Error, the meta file does not exist.");
			return(-1);
		};
	} else if(returnedValue < 0){
		perror("Error, impossible to remove the meta file.");
		return(-1);
	} else {
		if(wait(NULL) < 0){
			perror("Error, impossible to remove the meta file.");
			return(-1);
		}
	}
	return 0;
}

void freeMeta(int nbCommands, char** allowedCommands, char* secretCode){
	int i;
	for(i=0;i<nbCommands;i++){
		free(allowedCommands[i]);
	}
	free(allowedCommands);
	free(secretCode);
}