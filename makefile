leaSH: main.o readMeta.o game.o
	gcc -Wall -Wformat -Wextra -o leaSH main.o readMeta.o game.o -g -lreadline

main.o : main.c main.h
	gcc -Wall -Wformat -Wextra -o main.o -c main.c -g

readMeta.o : readMeta.c readMeta.h
	gcc -Wall -Wformat -Wextra -o readMeta.o -c readMeta.c -g

game.o : game.c game.h
	gcc -Wall -Wformat -Wextra -o game.o -c game.c -g