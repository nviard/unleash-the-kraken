/*  
	Copyright (C) 2014 TELECOM NANCY, <http://www.telecomnancy.eu>
		Damien CUPIF
		Nicolas VIARD
		
	This file is part of UnLeaSH The Kraken, a software meant to provide
	an educational shell helping the students in 1st year at TELECOM Nancy
	to understand the use of basics commands of Linux shell.

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERROR_H
#define ERROR_H

#define ERROR_ARGUMENTS 1
#define ERROR_LOADING_LEVEL 2
#define ERROR_READING_META 3
#define ERROR_DELETING_META 4
#define ERROR_RUNNING_GAME 5
#define ERROR_DELETING_GAME_DIRECTORY 6

#endif