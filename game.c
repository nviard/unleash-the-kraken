/*  
	Copyright (C) 2014 TELECOM NANCY, <http://www.telecomnancy.eu>
		Damien CUPIF
		Nicolas VIARD
		
	This file is part of UnLeaSH The Kraken, a software meant to provide
	an educational shell helping the students in 1st year at TELECOM Nancy
	to understand the use of basics commands of Linux shell.

	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <readline/readline.h>
#include <readline/history.h>


#include "error.h"
#include "game.h"
#include "design.h"

    static int readCommandLine(char*** arguments);
    static int executeCommandLine(int nbArguments, char**arguments, char* secretCode, char** allowedCommands, int nbAllowedCommands);
    static int isAuthorized(int nbCommands, char** allowedCommands, char**arguments);
    static void freeArguments(int nbArguments, char** arguments);
    static void killSons(int signal);
    static int defineSigactions();
    static char* newPath(char* cwd, int nbArgs, char**args);

    typedef struct
    {
    	int nbArgs;
    	char** args;
    	char* source;
    	char* destination;
    } COMMAND;

    int sonValue = 0;
    char* root = NULL;
    char* cwd = NULL;
    COMMAND* command = NULL;
    int nbCommands = 0;
/*
	Launch the level with right environnement
*/
	int game(int nbAllowedCommands, char** allowedCommands, char* secretCode) {

		//initial cwd
		cwd = (char*) malloc(2 * sizeof(char));
		cwd[0]='/';
		cwd[1]='\0';

		root = getcwd(root, 0);

		if(root == NULL){
			perror("Error, impossible to start the game.");
			return(-1);
		}

		if(defineSigactions() < 0){
			free(root);
			return(-1);
		}

		while (1) {
		// Prompt
			color("33");

			printf("\n> ");

		// Read the next command line entered
			char** arguments = NULL;
			int nbArguments = readCommandLine(&arguments);

			if(nbArguments < 0){
				free(cwd);
				free(root);
				return(-1);
			}

			if(nbArguments == 0){
				free(cwd);
				free(root);
				freeArguments(nbArguments, arguments);
				break;
			}

			if (arguments[0] != NULL && strcmp(arguments[0], "exit")==0) {
				free(cwd);
				free(root);
				freeArguments(nbArguments, arguments);
				break;
			}

			int returnValue = executeCommandLine(nbArguments, arguments,secretCode, allowedCommands, nbAllowedCommands);

			{
				int nComm;
				for(nComm = 0; nComm < nbCommands; nComm ++){
					free(command[nComm].args);
				}
				free(command);
			}

			freeArguments(nbArguments, arguments);

			if(chdir(root) < 0){
				perror("Error, impossible to launch the wished command.");
				freeArguments(nbArguments, arguments);
				free(cwd);
				free(root);
				return(-1);
			}	

			if(returnValue < 0){
				free(cwd);
				free(root);
				return(-1);
			} else if (returnValue){
				free(cwd);
				free(root);
				color("1;32");
				printf("\nCongratulations ! You finally managed to find the secret code !\n");
				break;
			}

		}
		color("1;36");
		printf("The Kraken hopes you enjoyed the lesson, see you soon son !\n");
		color("0");

		return(0);
	}

/*
	Reads the command line entered by player
*/
	static int readCommandLine(char*** arguments) {
		char* commandEnteredbyUser = NULL;
		size_t len = 0;
		ssize_t lenRead = 0;
		char* currentToken = NULL;

		*arguments = (char**) malloc(sizeof(char*));

		if(*arguments == NULL){
			perror("Error, impossible to read the command line.");
			return(-1);
		}

		lenRead = getline(&commandEnteredbyUser, &len, stdin);

		//if you use Ctrl+D
		if(lenRead <= 0){
		 	free(commandEnteredbyUser);
		 	return(0);
		}

		commandEnteredbyUser[lenRead - 1] = '\0';

		int nbArguments = 1;

		currentToken = strtok(commandEnteredbyUser, " ");
		
		if(currentToken != NULL)
			(*arguments)[0] = strdup(currentToken);
		else
			(*arguments)[0] = NULL;

		while (currentToken!=NULL ) {
			nbArguments++;
			*arguments = (char**) realloc(*arguments, nbArguments * sizeof(char*));
			
			if(*arguments == NULL){
				perror("Error, impossible to read the command line.");
				return(-1);
			}
			
			currentToken = strtok(NULL, " ");
			if(currentToken != NULL)
				(*arguments)[nbArguments - 1] = strdup(currentToken);
			else
				(*arguments)[nbArguments - 1] = NULL;
		}

		free(commandEnteredbyUser);
		return(nbArguments);
	}

	static int executeCommandLine(int nbArguments, char**arguments, char* secretCode, char** allowedCommands, int nbAllowedCommands){

		char* response = NULL;
		char* error = NULL;

		nbCommands = 1;

		command = (COMMAND*) malloc(sizeof(COMMAND));
		command[nbCommands - 1].nbArgs = 0;
		command[nbCommands - 1].args = NULL;
		command[nbCommands - 1].source = NULL;
		command[nbCommands - 1].destination = NULL;

		{
			int currentArg = 0;
			while(currentArg < nbArguments - 1){
				if(strcmp(arguments[currentArg], ">") == 0){
					currentArg ++;
					command[nbCommands - 1].destination = arguments[currentArg];
				} else if(strcmp(arguments[currentArg], "<") == 0){
					currentArg ++;
					command[nbCommands - 1].source = arguments[currentArg];
				} else if(strcmp(arguments[currentArg], "|") == 0){
					command[nbCommands - 1].nbArgs ++;
					command[nbCommands - 1].args = (char**) realloc (command[nbCommands - 1].args, command[nbCommands - 1].nbArgs * sizeof(char*));
					command[nbCommands - 1].args[command[nbCommands - 1].nbArgs - 1] = NULL;
					nbCommands ++;
					command = (COMMAND*) realloc(command, nbCommands * sizeof(COMMAND));
					command[nbCommands - 1].nbArgs = 0;
					command[nbCommands - 1].args = NULL;
					command[nbCommands - 1].source = NULL;
					command[nbCommands - 1].destination = NULL;
				} else {
					{
						//TODO
					}
					command[nbCommands - 1].nbArgs ++;
					command[nbCommands - 1].args = (char**) realloc (command[nbCommands - 1].args, command[nbCommands - 1].nbArgs * sizeof(char*));
					command[nbCommands - 1].args[command[nbCommands - 1].nbArgs - 1] = arguments[currentArg];
				}
				currentArg ++;
			}
			command[nbCommands - 1].nbArgs ++;
			command[nbCommands - 1].args = (char**) realloc (command[nbCommands - 1].args, command[nbCommands - 1].nbArgs * sizeof(char*));
			command[nbCommands - 1].args[command[nbCommands - 1].nbArgs - 1] = NULL;
		}
		

		int nComm;

		int fdpipe[2];

		for(nComm=0; nComm < nbCommands - 1; nComm ++){

			if (!isAuthorized(nbAllowedCommands, allowedCommands, arguments)) {
				color("31");
				printf("This command is not authorized, please try another ...");
				return(0);
			}

			if(pipe(fdpipe) < 0){
				perror("Error, impossible to launch the execution of the command.");
				return(-1);	
			}

			sonValue = fork();
			if(sonValue == 0){
				dup2(fdpipe[1], 1);
				if(close(fdpipe[0]) < -1){
					perror("Error, impossible to retrieve the error code of the command.");
					return(-1);
				}
				if (command[nComm].args[0] != NULL && strcmp(command[nComm].args[0], "cd")==0) {
					char* tmp = cwd;
					cwd = newPath(cwd, command[nComm].nbArgs, command[nComm].args);

					char* currentPath = (char*) malloc((strlen(root) + strlen(cwd) + 1) * sizeof(char));
					strcpy(currentPath, root);
					strcat(currentPath, cwd);
					DIR * tmpDir = opendir (currentPath);
					if(tmpDir == NULL){
						color("31");
						printf("This directory does not exist, or you're not allowed to open it.");
						free(cwd);
						cwd = tmp;
					} else {
						free(tmpDir);
						free(tmp);
					}

					free(currentPath);

				} else if (command[nComm].args[0] != NULL && strcmp(command[nComm].args[0], "pwd")==0) {

					response = strdup(cwd);

				} else {

					{
						char* currentPath = (char*) malloc((strlen(root) + strlen(cwd) + 1) * sizeof(char));
						strcpy(currentPath, root);
						strcat(currentPath, cwd);
						if(chdir(currentPath) < 0){
							perror("Error, impossible to launch the wished command.");
							freeArguments(nbArguments, arguments);
							free(cwd);
							free(root);
							free(currentPath);
							return(-1);
						}
						free(currentPath);
					}

					if(nComm > 0){
						if(dup2(fdpipe[0], 0) < 0){
							perror("Error, impossible to launch the execution of the command.");
							return(-1);
						}
					} else if(command[nComm].source != NULL){
						int f = open(command[nComm].source, O_RDONLY);
						if(f < 0){
							perror("Error, impossible to launch the execution of the command.");
							return(-1);
						}
						if(dup2(f, 0) < 0){
							perror("Error, impossible to launch the execution of the command.");
							return(-1);
						}
					}

					if(execvp(command[nComm].args[0],command[nComm].args) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
				}
			} else if(sonValue > 0){
				wait(NULL);
				sonValue = 0;
			} else {
				perror("Error, impossible to launch the execution of the command.");
				return(-1);
			}
		}



		if (!isAuthorized(nbAllowedCommands, allowedCommands, arguments)) {
			color("31");
			printf("This command is not authorized, please try another ...");
			return(0);
		}

		if (command[nComm].args[0] != NULL && strcmp(command[nComm].args[0], "cd")==0) {
			char* tmp = cwd;
			cwd = newPath(cwd, command[nComm].nbArgs, command[nComm].args);

			char* currentPath = (char*) malloc((strlen(root) + strlen(cwd) + 1) * sizeof(char));
			strcpy(currentPath, root);
			strcat(currentPath, cwd);
			DIR * tmpDir = opendir (currentPath);
			if(tmpDir == NULL){
				color("31");
				printf("This directory does not exist, or you're not allowed to open it.");
				free(cwd);
				cwd = tmp;
			} else {
				free(tmpDir);
				free(tmp);
			}

			free(currentPath);

		} else if (command[nComm].args[0] != NULL && strcmp(command[nComm].args[0], "pwd")==0) {
			response = strdup(cwd);
		} else {

			{
				char* currentPath = (char*) malloc((strlen(root) + strlen(cwd) + 1) * sizeof(char));
				strcpy(currentPath, root);
				strcat(currentPath, cwd);
				if(chdir(currentPath) < 0){
					perror("Error, impossible to launch the wished command.");
					freeArguments(nbArguments, arguments);
					free(cwd);
					free(root);
					free(currentPath);
					return(-1);
				}
				free(currentPath);
			}

			int fd[2];
			int fderror[2];

			if(pipe(fd) < 0){
				perror("Error, impossible to execute the command.");
				return(-1);
			}

			if(pipe(fderror) < 0){
				perror("Error, impossible to execute the command.");
				return(-1);
			}

			sonValue = fork();

			if (sonValue==0) {
				if(nComm > 0){
					if(close(fdpipe[1]) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
					if(dup2(fdpipe[0], 0) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
				} else if(command[nComm].source != NULL){
					int f = open(command[nComm].source, O_RDONLY);
					if(f < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
					if(dup2(f, 0) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
				}
				if(command[nComm].destination != NULL){
					int f = open(command[nComm].destination, O_WRONLY | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO);
					if(f < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
					if(dup2(f, 1) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
					if(close(fd[1]) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
				} else {
					if(dup2(fd[1],1) < 0){
						perror("Error, impossible to launch the execution of the command.");
						return(-1);
					}
				}				
				if(close(fd[0]) < 0){
					perror("Error, impossible to launch the execution of the command.");
					return(-1);
				}
				if(close(fderror[0]) < 0){
					perror("Error, impossible to launch the execution of the command.");
					return(-1);
				}
				if(dup2(fderror[1], 2) < 0){
					perror("Error, impossible to launch the execution of the command.");
					return(-1);
				}
				if(execvp(command[nComm].args[0],command[nComm].args) < 0){
					perror("Error, impossible to launch the execution of the command.");
					return(-1);
				}
									
			} else if(sonValue > 0){
				if(close(fd[1]) < 0){
					perror("Error, impossible to execute the command.");
					return(-1);
				}
				if(close(fderror[1]) < 0){
					perror("Error, impossible to execute the command.");
					return(-1);
				}
				wait(NULL);

				sonValue = 0;

				int i = 0;
				char c;

				while(read(fd[0], &c, 1) > 0){
					i ++;
					response = (char*) realloc(response, i * sizeof(char));
					if(response == NULL){
						perror("Error, impossible to retrieve the command's result.");
						return(-1);
					}
					response[i -1] = c;
				}

				if(response != NULL){
					response[i -1] = '\0';
				}

				if(close(fd[0]) < 0){
					perror("Error, impossible to retrieve the command's result.");
					return(-1);
				}

				i = 0;
				while(read(fderror[0], &c, 1) > 0){
					i ++;
					error = (char*) realloc(error, i * sizeof(char));
					if(error == NULL){
						perror("Error, impossible to retrieve the error code of the command.");
						return(-1);
					}
					error[i -1] = c;
				}

				if(error != NULL){
					error[i -1] = '\0';
				}

				if(close(fderror[0]) < 0){
					perror("Error, impossible to retrieve the error code of the command.");
					return(-1);
				}

			} else {
				perror("Error, impossible to execute the command.");
				return(-1);
			}
		}

		if(response == NULL){
			response = (char*) malloc(sizeof(char));
			response[0] = '\0';
			if(response == NULL){
				perror("Error, impossible to retrieve the command's result.");
				return(-1);
			}
		}

		if(error == NULL){
			error = (char*) malloc(sizeof(char));
			error[0] = '\0';
			if(error == NULL){
				perror("Error, impossible to retrieve the error code of the command.");
				return(-1);
			}
		}

		printf("%s", response);
		printf("%s", error);

		if (strcmp(response,secretCode)==0 || strcmp(error,secretCode)==0) {
			free(response);
			free(error);
			return(1);
		}


		// We need to reinitialize the response for a further command execution
		free(response);
		free(error);

		return(0);
	}

/*
	Check if the entered command is allowed for use
*/
	static int isAuthorized(int nbCommands, char** allowedCommands, char** arguments) {
		int i = 0;

		while (i<nbCommands) {
			if (arguments[0] != NULL && strcmp(allowedCommands[i],arguments[0])==0) {
				return(1);
			}
			i++;
		}

		return(0);
	}

	static void freeArguments(int nbArguments, char** arguments){
		int i;
		for(i=0;i<nbArguments;i++){
			free(arguments[i]);
		}
		free(arguments);
	}

	static void killSons(int signal){
		if(sonValue > 0){
			kill(sonValue, signal);
		}
	}

	static int defineSigactions(){
		struct sigaction action;
		action.sa_handler = &killSons;
		action.sa_flags = 0;
		if(sigemptyset(&action.sa_mask) < 0){
			perror("Error, impossible to manage the signals.");
			return(-1);
		}
		if(sigaction (SIGINT, &action, NULL) < 0){
			perror("Error, impossible to manage the signals.");
			return(-1);
		}
		return(0);
	}

	static char* newPath(char* cwd, int nbArgs, char**args){
		char* path = NULL;
		if(nbArgs < 3){
			path = (char*) malloc(2 * sizeof(char));
			path[0]='/';
			path[1]='\0';
		} else {
			if(args[1][0] == '/'){
				path = (char*) malloc(2 * sizeof(char));
				path[0]='/';
				path[1]='\0';
			} else {
				path = strdup(cwd);
			}
			int currentLen;
			int tokenLen;

			char* token = strtok(args[1], "/");
			
			if(token != NULL){
				do{
					currentLen = strlen(path);
					tokenLen = strlen(token);
					if(strcmp(token, "..")==0){
						int i = currentLen -2;
						while(i > 0 && path[i] != '/'){
							i --;
						}
						if(i < 0){
							color("31");
							printf("Error, impossible to leave the current directory.");
							free(path);
							path = strdup(cwd);
						} else {
							path = (char*) realloc(path, (i+2) * sizeof(char));
							path[i+1] = '\0';
						}	
					} else if(strcmp(token, ".")!=0 && token[0] != '/'){
						//2 for directory separator and final \n
						path = (char*) realloc(path, (currentLen + tokenLen + 2) * sizeof(char));
						path = strcat(path, token);
						path = strcat(path, "/");
					}
					token = strtok(NULL, "/");
				} while(token != NULL);
			}

		}
		return path;
	}